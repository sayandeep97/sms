package com.studentApp.StudentManagement.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.studentApp.StudentManagement.entity.Course;
import com.studentApp.StudentManagement.repository.CourseRepository;
import com.studentApp.StudentManagement.repository.StudentRepository;

@RestController
public class CourseController {
	
	@Autowired
	CourseRepository crepo;
	
	@Autowired
	StudentRepository repo;
	
	@GetMapping("/allCourses")
	public List <Course> getAllCourses()
	{
		List <Course> c = crepo.findAll();
		return c;
	}

	@PostMapping("/createCourse")
	public String createCourse(@RequestBody Course c)
	{
		crepo.save(c);
		return "course added successfully";
	}
	
	@GetMapping("/courseById/{id}")
	public Optional <Course> getCourseById(@PathVariable int id)
	{
		Optional <Course> course = crepo.findById(id);
		return course;
	}
	
	@GetMapping("/courseByStudentId/{studentId}")
	public Course getCourseByStudentId(@PathVariable("studentId") int studentId)
	{
		Course c = crepo.findByStudentsId(studentId);
		return c;
	}
	
	@GetMapping("/courseByStudentName")
	public Course getCourseByStudentName(@RequestParam("first_name") String firstName)
	{
		Course c = crepo.findByStudentsFirstName(firstName);
		return c;
	}
	
	@DeleteMapping("/removeCourse/{id}")
	public String removeCourse(@PathVariable int id)
	{
		crepo.deleteById(id);
		return "course deleted successfully";
	}
}
