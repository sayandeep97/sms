package com.studentApp.StudentManagement.controller;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.studentApp.StudentManagement.entity.Course;
import com.studentApp.StudentManagement.entity.Student;
import com.studentApp.StudentManagement.repository.CourseRepository;
import com.studentApp.StudentManagement.repository.StudentRepository;

@RestController
public class StudentController {
	
	@Autowired
	StudentRepository repo;
	
	@Autowired
	CourseRepository crepo;
	
	@GetMapping("/student/{id}")
	public Optional <Student> getStudentById(@PathVariable int id)
	{
		Optional <Student> s = repo.findById(id);
		return s;
	}

	@GetMapping("/allStudents")
	public List <Student> getAllStudents()
	{
		List <Student> all = repo.findAll();
		return all;
	}
	
	@GetMapping("/studentsByCourse/{courseId}")
	public List <Student> getStudentsByCourse(@PathVariable("courseId") int courseId)
	{
		List <Student> studentsByCourse = repo.findByCourseId(courseId);
		return studentsByCourse;
	}
	
	@GetMapping("/numberOfStudentsByCourse/{courseId}")
	public String getNumberOfStudentsByCourse(@PathVariable("courseId") int courseId)
	{
		List <Student> studentsByCourse = repo.findByCourseId(courseId);
		Optional <Course> c = crepo.findById(courseId);
		return ("Number of students enrolled for " + c + "is : " + studentsByCourse.size());
	}
	
	@Transactional
	@PostMapping("/registerStudent")
	public String registerStudent(@RequestBody Student st, @RequestParam("course_name") String name)
	{
		Course course = crepo.findByName(name);
		st.setCourse(course);
		repo.save(st);
		return ("student registered succesfully to course : " + name);
	}
	
	@DeleteMapping("/removeStudent/{id}")
	public String removeStudent(@PathVariable int id)
	{
		repo.deleteById(id);
		return "student deleted successfully";
	}
}
