package com.studentApp.StudentManagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.studentApp.StudentManagement.entity.Course;

@Repository
public interface CourseRepository extends JpaRepository<Course, Integer> {

	Course findByStudentsId(int studentId);
	
	Course findByName(String name);
	
	Course findByStudentsFirstName(String firstName);
}
