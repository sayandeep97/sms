package com.studentApp.StudentManagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.studentApp.StudentManagement.entity.Student;

@Repository
public interface StudentRepository extends JpaRepository<Student, Integer> {
	
	public List <Student> findByCourseId(int courseId);

}
